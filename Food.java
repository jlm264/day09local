public class Food
{
    private String description;
    private int calories;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    /**
     * param@ new desc
     */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    public int getCals(){
        return calories;
    }
    //casting
    Wings wingRef = (Wings)snacks[0];
    @override
    public String toString(){
        return description;
    }
    
}